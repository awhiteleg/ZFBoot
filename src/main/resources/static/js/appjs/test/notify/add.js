$().ready(function() {
    loadType();
	validateRule();

});

$.validator.setDefaults({
	submitHandler : function() {
		save();
	}
});
function save() {
    var status;
    if($("#status").is(":checked")){
        status="";
    }else{
        status="&status=0";
    }
	$.ajax({
		cache : true,
		type : "POST",
		url : "/test/notify/save",
		data : $('#signupForm').serialize()+status,// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});
    fileUpload();
}

function loadType(){
    var html = "";
    $.ajax({
        url : '/common/dict/list/oa_notify_type',
        success : function(data) {
            //加载数据
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].value + '">' + data[i].name + '</option>'
            }
            $(".chosen-select").append(html);
            $(".chosen-select").chosen({
                maxHeight : 200
            });
            //点击事件
            $('.chosen-select').on('change', function(e, params) {
                console.log(params.selected);
                var opt = {
                    query : {
                        type : params.selected,
                    }
                }
                $('#exampleTable').bootstrapTable('refresh', opt);
            });
        }
    });
}
function fileUpload(){
    var formData = new FormData();
    var files = $('#files')[0].files;
    for(var i=0; i<files.length; i++){
        var file = files[i];
        formData.append("files",file);
	}
    //formData.append("files2", $('#files2')[0].files[0]);
    $.ajax({
        url: '/common/sysFile/uploadMult' ,
        type: 'post',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        async: false
    }).done(function(res) {

    }).fail(function(res) {

    });
}

function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
			type : {
				required : true,
				digits : true,
				maxlength: 1
			}
		},
		messages : {
			type : {
				required : icon + "请输入类型",
			}
		}
	})
}