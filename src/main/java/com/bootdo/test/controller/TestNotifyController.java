package com.bootdo.test.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bootdo.test.domain.TestNotifyDO;
import com.bootdo.test.service.TestNotifyService;
import com.bootdo.common.utils.PageUtils;
import com.bootdo.common.utils.Query;
import com.bootdo.common.utils.R;

/**
 * 通知通告
 * 
 * @author zhaoming
 * @email dmwwmd23321@163.com
 * @date 2018-06-26 10:46:39
 */
 
@Controller
@RequestMapping("/test/notify")
public class TestNotifyController {
	@Autowired
	private TestNotifyService notifyService;
	
	@GetMapping()
	@RequiresPermissions("test:notify:notify")
	String Notify(){
	    return "test/notify/notify";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("test:notify:notify")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<TestNotifyDO> notifyList = notifyService.list(query);
		int total = notifyService.count(query);
		PageUtils pageUtils = new PageUtils(notifyList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("test:notify:add")
	String add(){
	    return "test/notify/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("test:notify:edit")
	String edit(@PathVariable("id") Long id,Model model){
		TestNotifyDO notify = notifyService.get(id);
		model.addAttribute("notify", notify);
	    return "test/notify/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("test:notify:add")
	public R save( TestNotifyDO notify){
		if(notifyService.save(notify)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("test:notify:edit")
	public R update( TestNotifyDO notify){
		notifyService.update(notify);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("test:notify:remove")
	public R remove( Long id){
		if(notifyService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("test:notify:batchRemove")
	public R remove(@RequestParam("ids[]") Long[] ids){
		notifyService.batchRemove(ids);
		return R.ok();
	}
	
}
