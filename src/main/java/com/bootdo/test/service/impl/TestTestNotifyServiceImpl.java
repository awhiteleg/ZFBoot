package com.bootdo.test.service.impl;

import com.bootdo.test.domain.TestNotifyDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.bootdo.test.dao.TestNotifyDao;
import com.bootdo.test.service.TestNotifyService;



@Service
public class TestTestNotifyServiceImpl implements TestNotifyService {
	@Autowired
	private TestNotifyDao testNotifyDao;
	
	@Override
	public TestNotifyDO get(Long id){
		return testNotifyDao.get(id);
	}
	
	@Override
	public List<TestNotifyDO> list(Map<String, Object> map){
		return testNotifyDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return testNotifyDao.count(map);
	}
	
	@Override
	public int save(TestNotifyDO notify){
		return testNotifyDao.save(notify);
	}
	
	@Override
	public int update(TestNotifyDO notify){
		return testNotifyDao.update(notify);
	}
	
	@Override
	public int remove(Long id){
		return testNotifyDao.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return testNotifyDao.batchRemove(ids);
	}
	
}
