package com.bootdo.test.service;

import com.bootdo.test.domain.TestNotifyDO;

import java.util.List;
import java.util.Map;

/**
 * 通知通告
 * 
 * @author zhaoming
 * @email dmwwmd23321@163.com
 * @date 2018-06-26 10:46:39
 */
public interface TestNotifyService {
	
	TestNotifyDO get(Long id);
	
	List<TestNotifyDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(TestNotifyDO notify);
	
	int update(TestNotifyDO notify);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
